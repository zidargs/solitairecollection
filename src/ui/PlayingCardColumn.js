import Template from "/src/util/Template.js";

const TPL = new Template(`
    <style>
        * {
            position: relative;
            box-sizing: border-box;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }
        :host {
            display: flex;
            flex-direction: column;
            align-items: center;
            width: calc(8vw * var(--card-scale, 1));
        }
        #face {
            position: absolute;
            width: calc(8vw * var(--card-scale, 1));
            height: calc(12vw * var(--card-scale, 1));
            border-radius: calc(1vw * var(--card-scale, 1));
            box-shadow: inset 0px 0px 0px 4px rgba(255,255,255,0.5);
        }
        ::slotted(cgc-playingcard) {
            flex: 1;
            max-height: 2.5vmax;
        }
        :host(.grabbed) {
            z-ndex: 1000;
            pointer-events: none;
            touch-action: none;
        }
        :host(.grabbed.floating) {
            position: absolute;
        }
        :host(.grabbed) ::slotted(cgc-playingcard) {
            cursor: grabbing;
        }
    </style>
    <div id="face">
    </div>
    <slot>
    </slot>
`);

export default class PlayingCardColumn extends HTMLElement {

    constructor() {
        super();
        this.attachShadow({mode: "open"});
        this.shadowRoot.append(TPL.generate());
    }

}

customElements.define("cgc-playingcardcolumn", PlayingCardColumn);
